package main

import (
	"testing"

	. "github.com/fgrosse/gomega-matchers"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/ghttp"
)

var testAPIKey = "ohvoh7thohf6ohT2"

func TestRedmine_TrackTime(t *testing.T) {
	RegisterExtendedTestingT(t)

	server := NewServer()
	defer server.Close()

	r, err := NewRedmineTracker(server.URL(), testAPIKey)
	Expect(err).NotTo(HaveOccurred())
	r.TimeEntryActivityID = 100

	server.AppendHandlers(CombineHandlers(
		VerifyRequest("POST", "/time_entries.json"),
		VerifyJSON(`{
			"time_entry": {
				"issue_id":    42,
				"activity_id": 100,
				"spent_on":    "2006-04-17",
				"hours":       1.25,
				"comments":    "stay frosty"
			}
		}`),
		RespondWith(201, `{
			"time_entry": {
				"id": 123,
				"issue_id":    42,
				"activity_id": 100,
				"spent_on":    "2006-04-17",
				"hours":       1.25,
				"comments":    "stay frosty"
			}
		}`),
	))

	a := &Activity{
		Name:    "stay frosty",
		IssueID: "42",
		Begin:   testTime("10:00"),
		End:     testTime("11:15"),
	}

	Expect(r.TrackTime(a)).To(Succeed())
	Expect(server.ReceivedRequests()).To(HaveLen(1))
	Expect(a.TimeEntryID).To(Equal("123"))
}
