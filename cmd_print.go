package main

import (
	"os"

	"fmt"
	"time"

	"github.com/alecthomas/template"
	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(printCmd)
}

var printCmd = &cobra.Command{
	Use:   "print <format>",
	Short: "Print information about tracked activities",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			Error.Fatal(cmd.Help())
		}

		funcMap := template.FuncMap{
			"date": func(layout string, t time.Time) string {
				return t.Format(layout)
			},
			"duration": func(a Activity) string {
				d := a.Duration()
				switch {
				case d < time.Minute:
					return fmt.Sprintf("%d seconds", int(d.Seconds()))
				case d < time.Hour:
					return fmt.Sprintf("%d min", int(d.Minutes()))
				default:
					return fmt.Sprintf("%.2f", d.Hours())
				}
			},
			"max_len": func(n int, s string) string {
				if len(s) > n  {
					s = s[0:n-3]+"..."
				}
				return s
			},
		}

		t, err := template.New("track").Funcs(funcMap).Parse(args[0])
		if err != nil {
			Error.Fatal(err)
		}

		db := Database()
		defer db.Close()

		var current Activity
		all := mustList(date, db)
		if len(all) > 0 {
			current = all[len(all)-1]
		}

		data := struct {
			All     []Activity
			Current Activity
		}{all, current}

		t.Execute(os.Stdout, data)
	},
}
