package main

import (
	"regexp"
	"testing"

	. "github.com/fgrosse/gomega-matchers"
	. "github.com/onsi/gomega"
	"github.com/spf13/viper"
)

func TestIssueIDFinder(t *testing.T) {
	RegisterExtendedTestingT(t)
	EnableTestLog(t)

	f := NewIssueIDFinder(map[string]Rule{
		"deployments": {Pattern: regexp.MustCompile("deploy"), IDs: []string{"123"}},

		// assign multiple IDs so the user can choose
		"help teammate": {Pattern: regexp.MustCompile("help"), IDs: []string{"1", "2", "3"}},

		// assign a priorities to create fallback rules that work if nothing matches
		"API service fallback": {Pattern: regexp.MustCompile("service"), IDs: []string{"789"}, Priority: -1},

		// mix submatch and normal issue id
		"fancy rule": {Pattern: regexp.MustCompile(`fancy (\d+)`), IDs: []string{"7", "$1", "42"}},
	})

	cases := map[string]struct {
		name     string
		issueIDs []string
	}{
		"empty":            {"", nil},
		"no match":         {"Hello World", nil},
		"simple":           {"#666", []string{"666"}},
		"simple 2":         {"#123 #456", []string{"123", "456"}},
		"simple with text": {"Hello World #123 This is a test #456", []string{"123", "456"}},
		"word match":       {"deploy new API", []string{"123"}},
		"word match 2":     {"help john with this thing", []string{"1", "2", "3"}},
		"prio match":       {"deploy a service", []string{"123"}}, // low prio rule does not match because higher prio rule
		"prio match 2":     {"develop the service", []string{"789"}},
		"multi matches":    {"help john to help alice to help mallory", []string{"1", "2", "3"}},
		"case insensitive": {"DEPLOY new API", []string{"123"}},

		"mix submatch and normal issue id": {"fancy 999", []string{"7", "999", "42"}},
	}

	for description, c := range cases {
		t.Logf("Running test case %q", description)
		actual := f.IssueIDs(Activity{Name: c.name})
		Expect(actual).To(Equal(c.issueIDs), description)
	}
}

func TestLoadRules(t *testing.T) {
	RegisterExtendedTestingT(t)
	EnableTestLog(t)

	v := viper.New()
	v.Set("rules", map[string]interface{}{
		"int id": map[string]interface{}{
			"pattern": "p1",
			"id":      1,
		},
		"string id": map[string]interface{}{
			"pattern": "p2",
			"id":      "2",
		},
		"int ids": map[string]interface{}{
			"pattern": "p1",
			"ids":     []int{3},
		},
		"string ids": map[string]interface{}{
			"pattern": "p2",
			"ids":     []string{"4"},
		},
		"mixed ids": map[string]interface{}{
			"pattern": "p2",
			"ids":     []interface{}{"5", 6},
		},
		"int prio": map[string]interface{}{
			"pattern": "p3",
			"id":      7,
			"prio":    -1,
		},
		"string prio": map[string]interface{}{
			"pattern": "p4",
			"id":      8,
			"prio":    "-1",
		},
	})

	rules, err := LoadRules(v)
	Expect(err).NotTo(HaveOccurred())

	check := func(key, pattern string, ids []string, prio int) {
		expected := Rule{
			Name:     key,
			Pattern:  regexp.MustCompile(pattern),
			IDs:      ids,
			Priority: prio,
		}

		Expect(rules).To(HaveKey(key))
		Expect(rules[key]).To(Equal(expected))
	}

	check("int id", "p1", []string{"1"}, 0)
}
