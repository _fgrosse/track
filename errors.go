package main

import "github.com/pkg/errors"

// ErrActivityNotStarted is the error that is returned if an activity's
// Stop method is called before Start.
var ErrActivityNotStarted = errors.New("activity has not been started")

var ErrActivityNotFound = errors.New("no such activity")
