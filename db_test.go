package main

import (
	"testing"
	"time"

	"github.com/benbjohnson/clock"
	. "github.com/fgrosse/gomega-matchers"
	. "github.com/onsi/gomega"
)

func TestDB_Add(t *testing.T) {
	RegisterExtendedTestingT(t)
	db := NewTestDB(t)
	defer db.MustClose()

	Expect(db.List(testDate)).To(BeEmpty())

	a := Activity{
		Name:  "TestDB_Add",
		Begin: testTime("10:00"),
		End:   testTime("10:30"),
	}

	Expect(db.Add(&a)).To(Succeed())
	Expect(db.List(testDate)).To(ContainElement(a))
}

func TestDB_Add_Overlapping(t *testing.T) {
	RegisterExtendedTestingT(t)
	db := NewTestDB(t)
	defer db.MustClose()

	a1 := Activity{
		Name:  "TestDB_Add_Overlapping_a1",
		Begin: testTime("10:00"),
		End:   testTime("12:00"),
	}
	a2 := Activity{
		Name:  "TestDB_Add_Overlapping_a2",
		Begin: testTime("11:00"),
		End:   testTime("11:30"),
	}

	Expect(db.Add(&a1)).To(Succeed())
	Expect(db.Add(&a2)).To(Succeed())

	activities, err := db.List(testDate)
	Expect(err).NotTo(HaveOccurred())

	Expect(activities[0].Name).To(Equal("TestDB_Add_Overlapping_a1"))
	Expect(activities[0].Begin).To(EqualTime(testTime("10:00")))
	Expect(activities[0].End).To(EqualTime(testTime("11:00")))

	Expect(activities[1].Name).To(Equal("TestDB_Add_Overlapping_a2"))
	Expect(activities[1].Begin).To(EqualTime(testTime("11:00")))
	Expect(activities[1].End).To(EqualTime(testTime("11:30")))
}

func TestDB_Add_AutoClosing(t *testing.T) {
	RegisterExtendedTestingT(t)
	db := NewTestDB(t)
	defer db.MustClose()

	a1 := Activity{
		Name:  "TestDB_Add_AutoClosing_a1",
		Begin: testTime("10:00"),
		// has not ended yet
	}
	a2 := Activity{
		Name:  "TestDB_Add_AutoClosing_a2",
		Begin: testTime("11:00"),
	}

	Expect(db.Add(&a1)).To(Succeed())
	Expect(db.Add(&a2)).To(Succeed())

	activities, err := db.List(testDate)
	Expect(err).NotTo(HaveOccurred())

	Expect(activities[0].Name).To(Equal("TestDB_Add_AutoClosing_a1"))
	Expect(activities[0].Begin).To(EqualTime(testTime("10:00")))
	Expect(activities[0].End).To(EqualTime(testTime("11:00")))
}

func TestDB_Delete(t *testing.T) {
	RegisterExtendedTestingT(t)
	db := NewTestDB(t)
	defer db.MustClose()

	a := Activity{Name: "TestDB_Delete", Begin: testDate, End: testDate.Add(time.Hour)}
	err := db.Add(&a)
	Expect(err).NotTo(HaveOccurred())

	ret, err := db.Delete(a.ID, testDate)
	Expect(ret).To(Equal(a))
	Expect(err).NotTo(HaveOccurred())

	_, err = db.Get(a.ID, testDate)
	Expect(err).To(MatchError(ErrActivityNotFound))
}

func TestDB_Stop(t *testing.T) {
	RegisterExtendedTestingT(t)
	db := NewTestDB(t)
	defer db.MustClose()

	a := Activity{Name: "TestDB_Stop", Begin: testDate}
	err := db.Add(&a)
	Expect(err).NotTo(HaveOccurred())

	Expect(db.Stop(testDate)).To(Succeed())

	actual, err := db.Get(a.ID, testDate)
	Expect(err).NotTo(HaveOccurred())
	Expect(actual.End.IsZero()).To(BeFalse(), "it should stop the currently running task")
}

func TestDB_Add_ComplexExample(t *testing.T) {
	RegisterExtendedTestingT(t)
	db := NewTestDB(t)
	defer db.MustClose()
	defer func() { Clock = clock.New() }() // restore the Clock

	now := testTime("09:39")
	clock := clock.NewMock()
	clock.Set(now)
	Clock = clock

	expected := []Activity{
		{
			Name:  "Do cool stuff",
			Begin: testTime("06:00"),
			End:   testTime("07:00"),
		},
		{
			Name:  "foo bar",
			Begin: testTime("07:00"),
			End:   testTime("08:15"),
		},
		{
			Name:  "test",
			Begin: testTime("08:15"),
		},
		{
			Name: "work work work!",
		},
	}

	for _, a := range expected {
		Expect(db.Add(&a)).To(Succeed())
	}

	expected[2].End = now
	expected[3].Begin = now

	actual, err := db.List(testDate)
	Expect(err).NotTo(HaveOccurred())
	Expect(actual).To(EqualActivities(expected))
}
