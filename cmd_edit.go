package main

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(editCmd)
}

var editCmd = &cobra.Command{
	Use:   "edit id <new activity name>",
	Short: "Edit an existing activity",
	Run: func(cmd *cobra.Command, args []string) {
		db := Database()
		defer db.Close()

		if len(args) < 2 {
			Error.Fatal(cmd.Help())
		}

		id, err := strconv.Atoi(args[0])
		if err != nil {
			FatalError(err)
		}

		a, err := db.Get(id, date)
		if err != nil {
			FatalError(err)
		}

		newActivity, err := parseActivity(strings.Join(args[1:], " "))
		if err != nil {
			FatalError(err)
		}

		var changed bool
		originalName := a.Name
		if newActivity.Name != "" {
			a.Name = newActivity.Name
			changed = true
		}

		if !newActivity.Begin.IsZero() {
			changed = true
			a.Begin = time.Date(
				a.Begin.Year(), a.Begin.Month(), a.Begin.Day(), newActivity.Begin.Hour(), newActivity.Begin.Minute(),
				0, 0, a.Begin.Location(),
			)
		}
		if !newActivity.End.IsZero() {
			changed = true
			a.End = time.Date(
				a.End.Year(), a.End.Month(), a.End.Day(), newActivity.End.Hour(), newActivity.End.Minute(),
				0, 0, a.End.Location(),
			)
		}

		if !changed {
			fmt.Println("Activity", a.ID, ":", white(originalName), "remains unchanged")
			fmt.Println()

			printAll(a.Begin, db)
			return
		}

		err = db.Save(a)
		if err != nil {
			FatalError(err)
		}

		fmt.Println("Activity", a.ID, ":", white(originalName), "was updated successfully")
		fmt.Println()

		printAll(a.Begin, db)
	},
}
