package main

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(listCmd)
}

var listCmd = &cobra.Command{
	Use:     "list [date]",
	Aliases: []string{"ls"},
	Short:   "Show all activities of a day",
	Run: func(cmd *cobra.Command, args []string) {
		db := Database()
		defer db.Close()

		if len(args) > 0 {
			date = mustParseDate(args[0])
		}

		printAll(date, db)
	},
}

func printAll(day time.Time, db *DB) {
	activities := mustList(day, db)
	printDateAndTotal(activities, date)

	for _, a := range activities {
		fmt.Printf("(%s) [%s] %s %s\n",
			gray(fmt.Sprintf("%d", a.ID)),
			gray(a.BeginEnd()),
			a.Icon(),
			white(a.Name),
		)
	}

	fmt.Println()
}

func printDateAndTotal(activities []Activity, day time.Time) {
	var total time.Duration
	for _, a := range activities {
		total = total + a.Duration()
	}

	fmt.Println(yellow("Date:  "), day.Format("Monday, 2006-01-02"))
	fmt.Println(yellow("Total: "), durationString(total))
	fmt.Println()
}

func mustList(day time.Time, db *DB) []Activity {
	activities, err := db.List(day)
	if err != nil {
		FatalError(err)
	}

	return activities
}

func durationString(d time.Duration) string {
	if d < time.Minute {
		return fmt.Sprintf("%.0f seconds", d.Seconds())
	}

	if d < time.Hour {
		return fmt.Sprintf("%.0f minutes", d.Minutes())
	}

	return fmt.Sprintf("%.2f hours", d.Hours())
}
