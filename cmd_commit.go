package main

import (
	"fmt"
	"io"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/_fgrosse/progress"
)

func init() {
	RootCmd.AddCommand(commitCmd)
}

var commitCmd = &cobra.Command{
	Use:   "commit [date]",
	Short: "Commit all tracked activities to redmine",
	Run: func(cmd *cobra.Command, args []string) {
		db := Database()
		defer db.Close()

		if len(args) > 0 {
			date = mustParseDate(args[0])
		}

		redmine, err := NewRedmineTracker(vpr.GetString("redmine.base_url"), vpr.GetString("redmine.api_key"))
		if err != nil {
			FatalError(errors.Wrap(err, "could not create redmine tracker"))
		}

		activities := mustList(date, db)
		printDateAndTotal(activities, date)

		forEach := func(f func(*Activity) error) {
			for i := range activities {
				a := &(activities[i])
				err = f(a)
				if err != nil {
					FatalError(err)
				}
			}
		}

		determineIssueIDs(forEach)

		n := 0
		pb := progress.Events{}

		pb.MustStart()
		pb.DisableScrollback = debug

		forEach(func(a *Activity) error {
			n++
			progress := pb.RegisterTask(commitStatusFunc(a))
			go commitActivity(a, progress, redmine, db)

			return nil
		})
		pb.WaitAndClose()

		fmt.Println()
		fmt.Println("Commited", white(fmt.Sprintf("%d activities", n)), "successfully") // TODO take errors into account
	},
}

func determineIssueIDs(forEach func(f func(*Activity) error)) {
	rules, err := LoadRules(vpr)
	if err != nil {
		Error.Fatal("Error while loading rules:", err.Error())
	}

	issueFinder := NewIssueIDFinder(rules)
	forEach(func(a *Activity) error {
		candidates := issueFinder.IssueIDs(*a)
		switch len(candidates) {
		case 0:
			// TODO: let the user create an issue on demand
			Debug.Printf("could not determine issue ID for activity %s", a.Name)
			return nil
		case 1:
			a.IssueID = candidates[0]
			return nil
		default:
			return fmt.Errorf("NOT IMPLEMENTED: let user choose an issue id out of %q", candidates)
		}
	})
}

func commitStatusFunc(a *Activity) func(message string, out io.Writer) {
	return func(message string, out io.Writer) {
		fmt.Fprintf(out, "(%s) [%s] %s %s : %s\n",
			gray(fmt.Sprintf("%d", a.ID)),
			gray(a.BeginEnd()),
			a.Icon(),
			white(a.Name),
			message,
		)
	}
}

func commitActivity(a *Activity, progress chan<- string, redmine *Redmine, db *DB) {
	defer close(progress)

	// TODO load issue ID to check if it exists and is assigned to this user (if not ask for confirmation)

	if a.IssueID == "" {
		// TODO ask later?
		progress <- "no issue ID"
		return
	}

	if a.TimeEntryID != "" {
		progress <- "issue has already been booked"
		return
	}

	progress <- fmt.Sprintf("Tracking time on #%s", white(a.IssueID))
	err := redmine.TrackTime(a)
	if err != nil {
		progress <- red(err.Error())
		return
	}

	progress <- fmt.Sprintf("Tracked %s on #%s", white(a.Duration().String()), white(a.IssueID))

	err = db.Save(*a)
	if err != nil {
		progress <- red(errors.Wrap(err, "activity was tracked to redmine but could not be updated in local database").Error())
	}
}
