package main

import "github.com/spf13/cobra"

func init() {
	RootCmd.AddCommand(stopCmd)
}

var stopCmd = &cobra.Command{
	Use:   "stop",
	Short: "Stop the currently running activity",
	Run: func(cmd *cobra.Command, args []string) {
		db := Database()
		defer db.Close()

		now := Clock.Now()
		err := db.Stop(now)
		if err != nil {
			FatalError(err)
		}

		printAll(now, db)
	},
}
