package main

import (
	"fmt"
	"time"

	"github.com/mgutz/ansi"
)

// Activity represents a named task the user has performed over a period of time.
// Activities have an easy to type ID so the user can reference individual IDs on the cli.
type Activity struct {
	ID         int
	Name       string
	Begin, End time.Time

	// IssueID is an ID in an issue tracking system (such das redmine)
	// that this activity relates to.
	IssueID string

	// TimeEntryID is the ID of the time entry if time was already booked
	// on this issue.
	TimeEntryID string
}

// Key returns the activity ID as string encoded in a slice of bytes.
func (a *Activity) Key() []byte {
	return []byte(fmt.Sprintf("%d", a.ID))
}

func (a Activity) Icon() string {
	switch {
	case a.TimeEntryID != "":
		return ansi.Color("✓", "green+b")
	default:
		return gray("»")
	}
}

// String returns a human readable string representation of this activity.
func (a *Activity) String() string {
	date := "-"
	if !a.Begin.IsZero() {
		date = a.Begin.Format("2006-01-02")
	}

	return fmt.Sprintf("[ID %s|%s|%s] %s",
		fmt.Sprintf("%d", a.ID),
		date,
		a.BeginEnd(),
		a.Name,
	)
}

// Start sets the begin time to now and clears the end time.
func (a *Activity) Start() {
	a.Begin = Clock.Now()
	a.End = time.Time{} // zero time
}

// Stop sets the end time to now.
// An error is returned if the activity has not been started before.
func (a *Activity) Stop() error {
	if a.Begin.IsZero() {
		return ErrActivityNotStarted
	}

	a.End = Clock.Now()
	return nil
}

// BeginEnd returns a string representation of this activity.
func (a *Activity) BeginEnd() string {
	b, e := "     ", "     "
	format := "%s-%s"

	if !a.Begin.IsZero() {
		b = a.Begin.Format("15:04")
	}
	if !a.End.IsZero() {
		e = a.End.Format("15:04")
	}

	if a.Begin.IsZero() || a.End.IsZero() {
		format = "%s %s"
	}

	return fmt.Sprintf(format, b, e)
}

// Duration returns how much time has passed between an activities Begin and End.
// If the activity has not been started or ended this returns zero.
func (a Activity) Duration() time.Duration {
	if a.Begin.IsZero() {
		return 0
	}

    if a.End.IsZero() {
        return Clock.Now().Sub(a.Begin)
    }

	return a.End.Sub(a.Begin)
}

// StatusIcon returns a colored unicode status icon meant to be displayed in a compatible shell.
func (a Activity) StatusIcon() string {
	switch {
	case a.End.IsZero():
		return ansi.Color("🕒", "yellow")
	default:
		return ansi.Color("✓", "green+b")

	}
}

func (a Activity) Overlaps(b Activity) bool {
	overlaps := func(a, b *Activity) bool {
		if (a.Begin == b.Begin || a.Begin.Before(b.Begin)) && b.Begin.Before(a.End) {
			// A: |-------|
			// B:   |---|
			return true
		}

		return false
	}

	return overlaps(&a, &b) || overlaps(&b, &a)
}

func (a Activity) IsZero() bool {
	return a.ID == 0
}

type Activities []Activity

func (p Activities) Len() int           { return len(p) }
func (p Activities) Less(i, j int) bool { return p[i].Begin.Before(p[j].Begin) }
func (p Activities) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
