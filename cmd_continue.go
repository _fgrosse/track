package main

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(continueCmd)
}

var continueCmd = &cobra.Command{
	Use:   "continue id",
	Short: "Start a new activity using the name of an existing activity",
	Run: func(cmd *cobra.Command, args []string) {
		db := Database()
		defer db.Close()

		if len(args) < 1 {
			Error.Fatal(cmd.Help())
		}

		id, err := strconv.Atoi(args[0])
		if err != nil {
			FatalError(err)
		}

		date := Clock.Now()
		existing, err := db.Get(id, date)
		if err != nil {
			FatalError(err)
		}

		a := Activity{Name: existing.Name}
		err = db.Add(&a)
		if err != nil {
			FatalError(err)
		}

		fmt.Println("Activity", a.ID, ":", white(a.Name), "has been continued successfully")
		fmt.Println()

		printAll(a.Begin, db)
	},
}
