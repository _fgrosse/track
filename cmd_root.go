package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/benbjohnson/clock"
	"github.com/mgutz/ansi"
	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	debug   bool
	date    time.Time
	vpr     = viper.New()

	white  = ansi.ColorFunc("white+bh")
	gray   = ansi.ColorFunc("241")
	yellow = ansi.ColorFunc("yellow+h")
	red    = ansi.ColorFunc("red+bh")

	Debug = log.New(ioutil.Discard, gray("Debug: "), log.Ltime|log.Lshortfile)
	Error = log.New(os.Stderr, red("Error: "), 0)

	Clock = clock.New()
)

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.track.yaml)")
	RootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "enable debug output")
	RootCmd.PersistentFlags().Var(dateFlagValue(&date, Clock.Now()), "date", "YYYY-MM-DD date you want to work on")
}

func FatalError(err error) {
	Error.Fatal(err)
	if debug {
		errors.Fprint(os.Stderr, err)
	}
}

var RootCmd = &cobra.Command{
	Use:   "track",
	Short: "The time tracker",
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func Database() *DB {
	db, err := NewDB(vpr.GetString("db"), Debug)
	if err != nil {
		Error.Fatal(err)
	}

	return db
}

func dateFlagValue(t *time.Time, value time.Time) *dateFlag {
	*t = value
	return &dateFlag{t}
}

// dateFlagValue implements the pflag.Value interface
type dateFlag struct {
	*time.Time
}

func (d *dateFlag) String() string {
	if d.Time.Format("2006-01-02") == Clock.Now().Format("2006-01-02") {
		return "today"
	}

	return d.Format("2006-01-02")
}

func (d *dateFlag) Set(s string) error {
	t, err := parseDate(s)
	if err != nil {
		return err
	}

	*d.Time = t
	return nil
}

func mustParseDate(s string) time.Time {
	d, err := parseDate(s)
	if err != nil {
		Error.Fatal(err)
	}

	return d
}

func parseDate(s string) (time.Time, error) {
	s = strings.ToLower(s)

	switch s {
	case "":
		return time.Time{}, fmt.Errorf("no value")
	case "now":
		fallthrough
	case "today":
		return Clock.Now(), nil
	case "yesterday":
		return Clock.Now().Add(-24 * time.Hour), nil
	}

	for d, dv := range daysOfWeek {
		if s == d || (len(s) >= 3 && s[:3] == d[:3]) {
			return lastWeekday(dv), nil
		}
	}

	// alternative format: "now-2" means "today minus two days"
	// short form: "t-2"
	if strings.HasPrefix(s, "now-") || strings.HasPrefix(s, "t-") {
		parts := strings.SplitN(s, "-", 2)
		n, err := strconv.Atoi(parts[1])
		if err != nil {
			return time.Time{}, errors.Wrap(err, "can not parse alternative format")
		}

		return Clock.Now().Add(time.Duration(-24*n) * time.Hour), nil
	}

	return time.Parse("2006-01-02", s)
}

var daysOfWeek = map[string]time.Weekday{
	"monday":    time.Monday,
	"tuesday":   time.Tuesday,
	"wednesday": time.Wednesday,
	"thursday":  time.Thursday,
	"friday":    time.Friday,
	"saturday":  time.Saturday,
	"sunday":    time.Sunday,
}

func lastWeekday(day time.Weekday) time.Time {
	t := Clock.Now()
	for {
		t = t.Add(-24 * time.Hour)
		if t.Weekday() == day {
			return t
		}
	}
}

func (d *dateFlag) Type() string {
	return "time.Time"
}
