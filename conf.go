package main

import (
	"log"
	"os"
	"strings"
)

func initConfig() {
	if debug {
		Debug = log.New(os.Stderr, gray("Debug: "), log.Ltime|log.Lshortfile)
	}

	if cfgFile != "" {
		vpr.SetConfigFile(cfgFile)
	}

	vpr.SetConfigName("config")
	vpr.AddConfigPath("$HOME/.track")
	vpr.AutomaticEnv()
	vpr.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	vpr.SetDefault("db", "$HOME/.track/bolt.db")

	if err := vpr.ReadInConfig(); err == nil {
		Debug.Println("Using config file:", vpr.ConfigFileUsed())
	}

	home := os.Getenv("HOME")
	if _, err := os.Stat(home + "/.track"); os.IsNotExist(err) {
		err = os.Mkdir(home+"/.track", 0770)
		if err != nil {
			Error.Fatal("Could not create directory: %s", err)
		}
	}
}
