package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/classmarkets/cmhttp"
	"github.com/pkg/errors"
)

type Redmine struct {
	HTTPClient cmhttp.Client

	// TimeEntryActivityID is the activity ID that is used when a new
	// Time Entry is posted to redmine. If the value is zero than no
	// activity ID is submitted thus using the configured default
	// of redmine.
	TimeEntryActivityID int
}

func NewRedmineTracker(baseURL, apiKey string) (*Redmine, error) {
	if baseURL == "" {
		return nil, fmt.Errorf("base URL can not be empty")
	}
	if apiKey == "" {
		return nil, fmt.Errorf("api key can not be empty")
	}

	c := cmhttp.Decorate(
		http.DefaultClient,
		logDecorator(),
		cmhttp.Scoped(baseURL),
		cmhttp.WithHeader("X-Redmine-API-Key", apiKey),
		cmhttp.JSON(),
	)

	return &Redmine{HTTPClient: c}, nil
}

func logDecorator() cmhttp.Decorator {
	return func(c cmhttp.Client) cmhttp.Client {
		return cmhttp.ClientFunc(func(r *http.Request) (res *http.Response, err error) {
			defer func(begin time.Time) {
				took := time.Since(begin)

				if err != nil {
					Error.Println(
						"Redmine HTTP client error",
						"method", r.Method,
						"url", r.URL,
						"request_content_length", r.Header.Get("Content-Length"),
						"took_ms", took/1e6,
						"error", err.Error(),
					)
				} else {
					respBody := &bytes.Buffer{}
					io.Copy(respBody, res.Body)
					res.Body.Close()
					res.Body = ioutil.NopCloser(respBody)

					Debug.Println(
						"Redmine HTTP client response",
						"method", r.Method,
						"url", r.URL,
						"response_status", res.Status,
						"took_ms", took/1e6,
						"response_body", respBody.String(),
					)
				}
			}(time.Now())

			res, err = c.Do(r)
			return res, err
		})
	}
}

func (t *Redmine) TrackTime(a *Activity) (err error) {
	if err := t.validate(a); err != nil {
		return errors.Wrap(err, "can not submit invalid activity to redmine")
	}

	type timeEntry struct {
		// request fields
		IssueID    int     `json:"issue_id"`
		Hours      float64 `json:"hours"`
		SpentOn    string  `json:"spent_on"`
		ActivityID int     `json:"activity_id,omitempty"`
		Comment    string  `json:"comments,omitempty"`

		// response fields
		ID int `json:"id,omitempty"`
	}

	numericIssueID, err := strconv.Atoi(a.IssueID)
	if err != nil {
		return errors.Wrapf(err, "invalid issue ID %q", a.IssueID)
	}

	if a.Duration() < 0 {
		return errors.Errorf("Invalid activity duration: %s %s-%s %q",
			a.Duration(), a.Begin.Format(time.RFC3339), a.End.Format(time.RFC3339), a.Name,
		)
	}

	body, err := json.Marshal(map[string]interface{}{
		"time_entry": &timeEntry{
			IssueID:    numericIssueID,
			Hours:      a.Duration().Hours(),
			SpentOn:    a.Begin.Format("2006-01-02"),
			ActivityID: t.TimeEntryActivityID,
			Comment:    a.Name,
		},
	})
	if err != nil {
		return errors.Wrap(err, "could not marshal request body")
	}

	Debug.Println("Request body:", string(body))
	req, err := http.NewRequest("POST", "/time_entries.json", bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "could not create HTTP request")
	}

	resp, err := t.HTTPClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "error while sending HTTP request")
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return errors.Errorf("bad HTTP response status %s", resp.Status)
	}

	var response struct {
		timeEntry `json:"time_entry"`
	}
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return errors.Wrap(err, "error while unmarshalling response body")
	}

	a.TimeEntryID = fmt.Sprintf("%d", response.ID)
	Debug.Printf("Issue (%d) was booked on redmine time entry %s", a.ID, a.TimeEntryID)

	return nil
}

func (r *Redmine) validate(a *Activity) error {
	switch {
	case a.Begin.IsZero():
		return errors.New("no start time")
	case a.End.IsZero():
		return errors.New("no end time")
	case strings.TrimSpace(a.Name) == "":
		return errors.New("empty name")
	case a.IssueID == "":
		return errors.New("empty issue ID")
	default:
		return nil
	}
}
