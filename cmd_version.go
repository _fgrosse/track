package main

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Show the version of this program",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(Version)
	},
}
