package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/fgrosse/gomega-matchers"
	"github.com/onsi/gomega"
	"github.com/onsi/gomega/types"
)

var testDate = time.Date(2006, 4, 17, 10, 21, 0, 0, time.UTC)

// DB is a test wrapper for DB.
type testDB struct {
	*DB
	path string
	t    *testing.T
}

func NewTestDB(t *testing.T) *testDB {
	f, err := ioutil.TempFile("", "track-test")
	if err != nil {
		panic(err)
	}

	if err := f.Close(); err != nil {
		panic(err)
	}

	path := f.Name()
	db, err := NewDB(path, nil)
	if err != nil {
		panic(err)
	}

	tdb := &testDB{DB: db, path: path, t: t}
	db.log = log.New(tdb, "test: ", log.Lshortfile)

	return tdb
}

func (db *testDB) Write(p []byte) (n int, err error) {
	s := string(p)
	db.t.Log(strings.TrimSpace(s))
	return len(p), nil
}

func (db *testDB) Close() error {
	defer os.Remove(db.path)
	return db.DB.Close()
}

func (db *testDB) MustClose() {
	if err := db.Close(); err != nil {
		panic(err)
	}
}

func testTime(s string) time.Time {
	d := testDate.Format("2006-01-02")
	t, err := time.Parse("2006-01-02 15:04", d+" "+s)
	if err != nil {
		panic(err)
	}

	return t
}

func EqualActivities(expected interface{}) types.GomegaMatcher {
	return &equalActivitiesMatcher{
		expected: expected.([]Activity),
		failed:   map[string]failedMatcher{},
	}
}

type failedMatcher struct {
	matcher types.GomegaMatcher
	actual  interface{}
}

type equalActivitiesMatcher struct {
	expected []Activity
	err      error
	failed   map[string]failedMatcher
}

func (m *equalActivitiesMatcher) Match(i interface{}) (success bool, err error) {
	actual, ok := i.([]Activity)
	if !ok {
		return false, fmt.Errorf("actual value must be a []Activity")
	}

	if len(actual) != len(m.expected) {
		return false, nil
	}

	for i := range m.expected {
		m.assert(i, "Name", gomega.Equal, m.expected[i].Name, actual[i].Name)
		m.assert(i, "IssueID", gomega.Equal, m.expected[i].IssueID, actual[i].IssueID)
		m.assert(i, "Begin", matchers.EqualTime, m.expected[i].Begin, actual[i].Begin)
		m.assert(i, "End", matchers.EqualTime, m.expected[i].End, actual[i].End)
	}

	return len(m.failed) == 0, m.err
}

type matcherFactory func(interface{}) types.GomegaMatcher

func (m *equalActivitiesMatcher) assert(i int, field string, f matcherFactory,
	expected, actual interface{}) {
	if m.err != nil {
		return
	}

	matcher := f(expected)
	ok, err := matcher.Match(actual)
	if !ok {
		msg := fmt.Sprintf("Activity %d does not match expectation for field %q", i+1, field)
		m.failed[msg] = failedMatcher{matcher, actual}
	}

	m.err = err
}

func (m *equalActivitiesMatcher) FailureMessage(_ interface{}) (message string) {
	var messages []string
	for msg, x := range m.failed {
		messages = append(messages, fmt.Sprintf("%s:\n%s", msg, x.matcher.FailureMessage(x.actual)))
	}

	return strings.Join(messages, "\n")
}

func (m *equalActivitiesMatcher) NegatedFailureMessage(_ interface{}) (message string) {
	var messages []string
	for msg, x := range m.failed {
		messages = append(messages, fmt.Sprintf("%s:\n%s", msg, x.matcher.NegatedFailureMessage(x.actual)))
	}

	return strings.Join(messages, "\n")
}

func EnableTestLog(t *testing.T) {
	Debug = log.New(&testLogger{t}, gray("Debug: "), log.LstdFlags|log.Lshortfile)
	Error = log.New(&testLogger{t}, red("Error: "), log.LstdFlags|log.Lshortfile)
}

// testLogger implements io.Writer so it can be used in a log.Logger
type testLogger struct{ *testing.T }

func (l *testLogger) Write(data []byte) (int, error) {
	l.Log(strings.TrimSpace(string(data)))
	return len(data), nil
}
