package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"time"

	"sort"

	"github.com/boltdb/bolt"
	"github.com/pkg/errors"
)

type DB struct {
	bolt *bolt.DB
	log  *log.Logger
}

// NewDB creates a new DB. If logger is nil all logs are discarded.
func NewDB(path string, logger *log.Logger) (*DB, error) {
	if logger == nil {
		logger = log.New(ioutil.Discard, "", 0)
	}

	path = os.ExpandEnv(path)
	logger.Printf("Opening new database at %q", path)
	db, err := bolt.Open(path, 0644, nil)

	return &DB{bolt: db, log: logger}, err
}

// Close implements the io.Closer interface.
func (db *DB) Close() error {
	return db.bolt.Close()
}

// Get returns a single Activity.
func (db *DB) Get(id int, date time.Time) (Activity, error) {
	result, err := db.List(date)
	if err != nil {
		return Activity{}, errors.Wrap(err, "could not list activities for date "+date.Format("2006-01-02"))
	}

	for i := range result {
		if result[i].ID == id {
			return result[i], nil
		}
	}

	return Activity{}, ErrActivityNotFound
}

// List returns all known activities for a given date sorted by date in ascending order.
func (db *DB) List(date time.Time) ([]Activity, error) {
	db.log.Println("Listing activities for", date.Format("2006-01-02"))
	activities := []Activity{}
	err := db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket(date))
		if b == nil {
			db.log.Println("Did not find a bucket for date", string(bucket(date)))
			return nil
		}

		return forEach(b, func(a Activity) error {
			activities = append(activities, a)
			return nil
		})
	})

	sort.Sort(Activities(activities))
	return activities, err
}

func forEach(b *bolt.Bucket, f func(a Activity) error) error {
	return b.ForEach(func(_, v []byte) error {
		var a Activity
		if err := json.Unmarshal(v, &a); err != nil {
			return err
		}

		return f(a)
	})
}

// Add adds the given activity to the database.
// If a has not been started yet it will be started.
// If a has not yet an ID the database will generate one.
// Adding an Activity without a name will cause an error.
func (db *DB) Add(a *Activity) error {
	db.log.Println("Adding activity", a)
	var err error
	if a.Name == "" {
		return errors.New("can not add an activity without a name")
	}

	if a.Begin.IsZero() {
		a.Start()
		db.log.Println("Setting activity start to", a.Begin.Format("15:04"))
	}

	if a.ID == 0 {
		a.ID, err = db.generateID(a.Begin)
		if err != nil {
			return errors.Wrap(err, "error while generating new ID")
		}
	}

	return db.bolt.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(bucket(a.Begin))
		if err != nil {
			return errors.Wrapf(err, "could not create bucket for time %s", a.Begin)
		}

		err = db.fixOverlaps(b, a)
		if err != nil {
			return errors.Wrap(err, "error while fixing overlaps")
		}

		err = db.autoClosePrevious(a, b)
		if err != nil {
			return errors.Wrap(err, "error while automatically ending previously running activity")
		}

		return db.save(*a, b)
	})
}

// TODO what about overlap from saving?
func (db *DB) Save(a Activity) error {
	if a.Name == "" {
		return errors.New("can not save activity without a name")
	}

	if a.Begin.IsZero() {
		return errors.New("can not save activity without a start time")
	}

	if a.ID == 0 {
		return errors.New("can not save activity without an ID")
	}

	return db.bolt.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists(bucket(a.Begin))
		if err != nil {
			return errors.Wrapf(err, "could not create bucket for time %s", a.Begin)
		}

		return db.save(a, b)
	})
}

func (db *DB) save(a Activity, b *bolt.Bucket) error {
	activity, err := json.Marshal(&a)
	if err != nil {
		return errors.Wrap(err, "error while marshalling activity")
	}

	db.log.Printf("Saving activity %s", activity)
	return b.Put(a.Key(), activity)
}

func (db *DB) fixOverlaps(b *bolt.Bucket, newActivity *Activity) error {
	return forEach(b, func(a Activity) error {
		if !newActivity.Overlaps(a) {
			return nil
		}

		if a.Begin.Before(newActivity.Begin) {
			db.log.Printf("Fixing overlap of activity (%d) and (%d) by setting (%d).End to (%d).Begin",
				a.ID, newActivity.ID, a.ID, newActivity.ID)
			a.End = newActivity.Begin
		} else {
			db.log.Printf("Fixing overlap of activity (%d) and (%d) by setting (%d).End to (%d).Begin",
				newActivity.ID, a.ID, newActivity.ID, a.ID)
			newActivity.End = a.Begin
		}

		return db.save(a, b)
	})
}

func (db *DB) autoClosePrevious(a *Activity, b *bolt.Bucket) error {
	lastActivity, err := lastActivityBefore(a, b)
	if err != nil {
		return err
	}

	if lastActivity.IsZero() || lastActivity.End.IsZero() == false {
		return nil
	}

	db.log.Printf("Automatically ending previously running activity (%d)", lastActivity.ID)
	lastActivity.End = a.Begin
	return db.save(lastActivity, b)
}

func lastActivityBefore(activity *Activity, b *bolt.Bucket) (Activity, error) {
	var lastActivity Activity
	err := forEach(b, func(a Activity) error {
		if a.Begin.After(lastActivity.Begin) && a.Begin.Before(activity.Begin) {
			lastActivity = a
		}

		return nil
	})

	return lastActivity, err
}

// Stop stops all activities that are currently running.
func (db *DB) Stop(date time.Time) error {
	db.log.Println("Stopping all running activities")

	activities, err := db.List(date)
	if err != nil {
		return errors.Wrap(err, "could not list running activities")
	}

	if len(activities) == 0 {
		db.log.Printf("There are no activities for date %s at all", date.Format("2006-01-02"))
		return nil
	}

	var stopped []Activity
	for _, a := range activities {
		if a.End.IsZero() {
			err := a.Stop()
			if err != nil {
				return errors.Wrapf(err, "could not stop activity %+v", a)
			}
			stopped = append(stopped, a)
		}
	}

	return db.bolt.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket(date))
		for _, a := range stopped {
			return db.save(a, b)
		}

		db.log.Printf("Successfully stopped %d activities", len(stopped))
		return nil
	})
}

func (db *DB) Delete(id int, date time.Time) (Activity, error) {
	db.log.Printf("Deleting activity %d at %s", id, date.Format("2006-01-02"))
	a, err := db.Get(id, date)
	if err != nil {
		return a, errors.Wrapf(err, "could not delete activity with ID %s at %s", id, date.Format("2006-01-02"))
	}

	return a, db.bolt.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucket(date))
		return bucket.Delete(a.Key())
	})
}

func (db *DB) ForEach(date time.Time, f func(Activity) error) error {
	return db.bolt.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucket(date))
		if b == nil {
			db.log.Println("Did not find a bucket for date", string(bucket(date)))
			return nil
		}

		return forEach(b, f)
	})
}

func bucket(t time.Time) []byte {
	return []byte(t.Format("2006-01-02"))
}

func (db *DB) generateID(date time.Time) (int, error) {
	db.log.Println("Generating next ID for date", date.Format("2006-01-02"))
	activities, err := db.List(date)
	if err != nil {
		return 0, errors.Wrap(err, "can not list activities")
	}

	var max int
	for i := range activities {
		if activities[i].ID > max {
			max = activities[i].ID
		}
	}

	id := max + 1
	db.log.Println("Generated ID is", id)
	return id, nil
}
