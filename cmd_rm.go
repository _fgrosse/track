package main

import (
	"strconv"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(rmCmd)
}

var rmCmd = &cobra.Command{
	Use:   "rm id1 ... idn",
	Short: "Remove an activity by ID",
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			Error.Fatal("need at least one argument")
		}

		db := Database()
		defer db.Close()

		date := Clock.Now()
		for i := range args {
			id, err := strconv.Atoi(args[i])
			if err != nil {
				FatalError(err)
			}

			_, err = db.Delete(id, date)
			if err != nil {
				FatalError(err)
			}
		}

		printAll(date, db)
	},
}
