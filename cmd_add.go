package main

import (
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(addCmd)
}

var addCmd = &cobra.Command{
	Use:   "add [from]-[to] [activity name]",
	Short: "Add a new activity",
	Run: func(cmd *cobra.Command, args []string) {
		db := Database()
		defer db.Close()

		input := strings.Join(args, " ")
		a, err := parseActivity(input)
		if err != nil {
			Error.Fatal(err)
		}

		err = db.Add(a)
		if err != nil {
			Error.Fatal(err)
		}

		fmt.Println("Activity", a.ID, ":", white(a.Name), "was added successfully")
		fmt.Println()

		printAll(a.Begin, db)
	},
}

var parseRE = regexp.MustCompile(`((\d\d:\d\d)(\s*-\s*(\d\d:\d\d|now))?)?\s*(.*)`)

func parseActivity(s string) (*Activity, error) {
	a := new(Activity)
	var err error

	matches := parseRE.FindStringSubmatch(s)
	if matches == nil {
		return a, fmt.Errorf("can not parse activity %q", s)
	}

	if matches[2] != "" {
		a.Begin, err = parseTime(matches[2])
		if err != nil {
			return a, fmt.Errorf("can not parse time <start>: %s", matches[2])
		}
	}

	if matches[4] != "" {
		if matches[4] == "now" {
			matches[4] = Clock.Now().Format("15:04")
		}

		a.End, err = parseTime(matches[4])
		if err != nil {
			return a, fmt.Errorf("can not parse time <end>: %s", matches[4])
		}
	}

	a.Name = matches[5]

	return a, nil
}

func parseTime(s string) (time.Time, error) {
	today := Clock.Now().Format("2006-01-02")
	return time.ParseInLocation("2006-01-02 15:04", today+" "+s, time.Local)
}
