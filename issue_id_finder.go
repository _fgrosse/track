package main

import (
	"math"
	"regexp"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type Rule struct {
	Name     string
	Pattern  *regexp.Regexp
	IDs      []string
	Priority int

	// err indicates if there was an error loading this rule
	err error
}

var defaultIssueIDRule = Rule{
	Pattern: regexp.MustCompile(`#(\d+)`),
	IDs:     []string{"$1"},
}

func LoadRules(v *viper.Viper) (map[string]Rule, error) {
	result := make(map[string]Rule)
	var rules map[string]map[string]interface{}

	err := v.UnmarshalKey("rules", &rules)
	if err != nil {
		return nil, err
	}

	for name, values := range rules {
		r := &Rule{Name: name}
		loadRulePattern(values, r)
		loadRuleID(values, r)
		loadRuleIDs(values, r)
		loadRulePrio(values, r)

		if r.err != nil {
			return nil, r.err
		}

		if len(r.IDs) == 0 {
			return nil, errors.Errorf("rule %q must have at least one ID", name)
		}

		Debug.Printf("Loaded rule %q: Pattern: %s, IDs: %q, Prio: %d", name, r.Pattern, r.IDs, r.Priority)
		result[name] = *r
	}

	return result, nil
}

func loadRulePattern(values map[string]interface{}, r *Rule) {
	if r.err != nil {
		return
	}

	pattern := values["pattern"]
	if pattern == "" {
		r.err = errors.Errorf("pattern of rule %q can not be empty", r.Name)
		return
	}

	p, ok := pattern.(string)
	if !ok {
		r.err = errors.Errorf("pattern of rule %q must be a string but is a %T", r.Name, pattern)
		return
	}

	r.Pattern, r.err = regexp.Compile(p)
	if r.err != nil {
		r.err = errors.Wrapf(r.err, "can not compile pattern regex of rule %q", r.Name)
	}
}

func loadRuleID(values map[string]interface{}, r *Rule) {
	if r.err != nil {
		return
	}

	if idX, ok := values["id"]; ok {
		id, err := loadString(idX)
		if err != nil {
			r.err = err
			return
		}
		r.IDs = append(r.IDs, id)
	}
}

func loadString(x interface{}) (string, error) {
	switch xx := x.(type) {
	case string:
		return xx, nil
	case int:
		return strconv.Itoa(xx), nil
	default:
		return "", errors.Errorf("must either be a string or an integer but is a %t", x)
	}
}

func loadRuleIDs(values map[string]interface{}, r *Rule) {
	if r.err != nil {
		return
	}

	if ids, ok := values["ids"]; ok {
		switch x := ids.(type) {
		case []string:
			r.IDs = append(r.IDs, x...)
		case []int:
			for _, intID := range x {
				r.IDs = append(r.IDs, strconv.Itoa(intID))
			}
		case []interface{}:
			for _, idX := range x {
				id, err := loadString(idX)
				if err != nil {
					r.err = err
					return
				}
				r.IDs = append(r.IDs, id)
			}
		}
	}
}

func loadRulePrio(values map[string]interface{}, r *Rule) {
	if r.err != nil {
		return
	}

	if prio, ok := values["prio"]; ok {
		switch x := prio.(type) {
		case string:
			r.Priority, r.err = strconv.Atoi(x)
		case int:
			r.Priority = x
		default:
			r.err = errors.Errorf("priority of rule %q must either be a string or an integer but is a %t", r.Name, prio)
		}
	}
}

type IssueIDFinder struct {
	rules map[string]Rule
}

func NewIssueIDFinder(rules map[string]Rule) *IssueIDFinder {
	rules["default"] = defaultIssueIDRule

	return &IssueIDFinder{rules}
}

func (f *IssueIDFinder) IssueIDs(a Activity) []string {
	candidates := map[int][]string{} // maps priorities to a slice of found issue IDs
	highestPrio := math.MinInt32

	for name, r := range f.rules {
		m := r.Pattern.FindAllStringSubmatch(strings.ToLower(a.Name), -1)
		if m == nil {
			continue
		}

		Debug.Printf("Rule %q matched: %+v => %+v (prio %d)", name, m, r.IDs, r.Priority)
		highestPrio = max(highestPrio, r.Priority)

		for _, id := range r.IDs {
			if !isSubmatchID(id) {
				candidates[r.Priority] = append(candidates[r.Priority], id)
				continue
			}

			idx, err := strconv.Atoi(id[1:])
			if err != nil {
				panic(errors.Errorf("can not parse numeric pattern index from submatch rule ID %q", id))
			}

			for i := range m {
				if len(m[i]) < idx {
					panic(errors.Errorf("submatch rule references index %d but there are only %d submatches", idx, len(m)))
				}
				candidates[r.Priority] = append(candidates[r.Priority], m[i][idx])
			}
		}
	}

	return candidates[highestPrio]
}

func isSubmatchID(id string) bool {
	return len(id) > 1 && id[0] == '$'
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
