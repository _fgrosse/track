package main

import (
	"testing"
	"time"

	"github.com/benbjohnson/clock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestActivity_Start(t *testing.T) {
	mockTime(testDate)
	defer unmockTime()

	a := Activity{}
	assert.True(t, a.Begin.IsZero())

	a.Start()
	assert.Equal(t, testDate, a.Begin)
}

func TestActivity_Stop(t *testing.T) {
	mockTime(testDate)
	defer unmockTime()

	a := Activity{}
	assert.True(t, a.End.IsZero())

	assert.Equal(t, a.Stop(), ErrActivityNotStarted)
	a.Begin = testDate.Add(-1 * time.Hour)

	require.NoError(t, a.Stop())
	assert.Equal(t, testDate, a.End)
}

func TestActivity_Overlaps(t *testing.T) {
	cases := map[string]struct {
		a, b     Activity
		overlaps bool
	}{
		"A immediately followed by B": {
			a:        Activity{Begin: testTime("15:00"), End: testTime("15:30")},
			b:        Activity{Begin: testTime("15:30"), End: testTime("16:00")},
			overlaps: false,
		},
		"A and B start a the same time but B takes longer": {
			a:        Activity{Begin: testTime("15:00"), End: testTime("15:30")},
			b:        Activity{Begin: testTime("15:00"), End: testTime("16:00")},
			overlaps: true,
		},
		"B is completely inside A": {
			a:        Activity{Begin: testTime("15:00"), End: testTime("15:30")},
			b:        Activity{Begin: testTime("15:10"), End: testTime("15:20")},
			overlaps: true,
		},
		"A ends before B starts": {
			a:        Activity{Begin: testTime("15:00"), End: testTime("15:30")},
			b:        Activity{Begin: testTime("17:00"), End: testTime("18:00")},
			overlaps: false,
		},
		"A ends before B starts and B does not end": {
			a:        Activity{Begin: testTime("15:00"), End: testTime("15:30")},
			b:        Activity{Begin: testTime("17:00")},
			overlaps: false,
		},
	}

	for description, c := range cases {
		ok := assert.Equal(t, c.overlaps, c.a.Overlaps(c.b), "%s\nA: %s\nB: %s",
			description, c.a.BeginEnd(), c.b.BeginEnd(),
		)
		if ok {
			assert.Equal(t, c.overlaps, c.b.Overlaps(c.a), "%s\nA: %s\nB: %s",
				description, c.b.BeginEnd(), c.a.BeginEnd(),
			)
		}
	}
}

func TestActivity_Duration(t *testing.T) {
	cases := []struct {
		begin, end time.Time
		expected   time.Duration
	}{
		{
			begin:    time.Time{},
			end:      time.Time{},
			expected: 0,
		},
		{
			begin:    time.Date(2016, 1, 1, 10, 00, 0, 0, time.UTC),
			end:      time.Time{},
			expected: 0,
		},
		{
			begin:    time.Date(2016, 1, 1, 10, 00, 0, 0, time.UTC),
			end:      time.Date(2016, 1, 1, 11, 00, 0, 0, time.UTC),
			expected: 1 * time.Hour,
		},
	}

	for _, c := range cases {
		a := Activity{Begin: c.begin, End: c.end}
		assert.Equal(t, c.expected, a.Duration())
	}
}

func mockTime(t time.Time) *clock.Mock {
	m := clock.NewMock()
	Clock = m

	if !t.IsZero() {
		m.Set(t)
	}

	return m
}

func unmockTime() {
	Clock = clock.New()
}
